
class Data{
  String author;
  String nameOfImage;
  String imageUrl;
  Data({this.author,this.imageUrl,this.nameOfImage});
  factory Data.fromJson(Map<String, dynamic> json) {
    return Data(
      author: json['user']['username'],
      nameOfImage: json['description'],
      imageUrl: json['urls']['small'],
    );
  }
}