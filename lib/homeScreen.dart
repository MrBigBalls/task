import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:test_project/fullScreenImage.dart';
import 'package:test_project/model/images.dart';

class HomeScreen extends StatefulWidget {
  static const route = 'homeScreen';
  static String choosenPhoto;

  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<Data> listOfImages = new List<Data>();
  Future<bool> fetchAlbum() async {
    final response = await http.get(
        'https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0');
    if (response.statusCode == 200) {
      List<dynamic> body = jsonDecode(response.body);
      for (var i = 0; i < body.length; i++) {
        listOfImages.add(Data.fromJson(body[i]));
      }
      return true;
    } else {
      print('Failed to load album');
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: FutureBuilder(
            future: fetchAlbum(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                  itemCount: listOfImages.length,
                  itemBuilder: (BuildContext ctx, int index) {
                    return Padding(
                        padding: EdgeInsets.all(20),
                        child: GestureDetector(
                          child: Card(
                            elevation: 20,
                            color: Colors.white,
                            child: Column(
                              children: <Widget>[
                                listOfImages[index].imageUrl != null
                                    ? Image.network(
                                        listOfImages[index].imageUrl)
                                    : Container(),
                                listOfImages[index].nameOfImage != null
                                    ? Row(children: <Widget>[
                                        SizedBox(width: 5),
                                        Expanded(
                                          child: Text(
                                            listOfImages[index].nameOfImage,
                                            style: TextStyle(fontSize: 25),
                                            softWrap: false,
                                            overflow: TextOverflow.fade,
                                          ),
                                        )
                                      ])
                                    : Container(),
                                listOfImages[index].author != null
                                    ? Row(children: <Widget>[
                                        SizedBox(width: 5),
                                        Expanded(
                                          child: Text(
                                            listOfImages[index].author,
                                            softWrap: false,
                                            overflow: TextOverflow.fade,
                                            style: TextStyle(
                                                fontSize: 15,
                                                color: Colors.grey),
                                          ),
                                        )
                                      ])
                                    : Container()
                              ],
                            ),
                          ),
                          onTap: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (_) {
                              return FullScreenImage(
                                imageUrl: listOfImages[index].imageUrl,
                                tag: index.toString(),
                              );
                            }));
                          },
                        ));
                  },
                );
              } else {
                return Container(
                  width: double.infinity,
                  height: double.infinity,
                  color: Colors.white,
                  child: Align(
                      alignment: Alignment.center,
                      child: CircularProgressIndicator()),
                );
              }
            }));
  }

  
}
